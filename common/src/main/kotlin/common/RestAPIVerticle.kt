package common

import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServer
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.CookieHandler
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.sstore.ClusteredSessionStore
import io.vertx.ext.web.sstore.LocalSessionStore
import java.util.HashSet


abstract class RestAPIVerticle : BaseMicroserviceVerticle() {

    /**
     * Create http server for the REST service.
     *
     * @param router router instance
     * @param host   http host
     * @param port   http port
     * @return async result of the procedure
     */
    protected fun createHttpServer(router: Router, host: String, port: Int): Future<Void> {
        val httpServerFuture = Future.future<HttpServer>()
        vertx.createHttpServer()
                .requestHandler { router.accept(it) }
                .listen(port, host, httpServerFuture.completer())
        return httpServerFuture.map { r -> null }
    }

    /**
     * Enable CORS support.
     *
     * @param router router instance
     */
    protected fun enableCorsSupport(router: Router) {
        val allowHeaders = HashSet<String>()
        allowHeaders.add("x-requested-with")
        allowHeaders.add("Access-Control-Allow-Origin")
        allowHeaders.add("origin")
        allowHeaders.add("Content-Type")
        allowHeaders.add("accept")
        val allowMethods = HashSet<HttpMethod>()
        allowMethods.add(HttpMethod.GET)
        allowMethods.add(HttpMethod.PUT)
        allowMethods.add(HttpMethod.OPTIONS)
        allowMethods.add(HttpMethod.POST)
        allowMethods.add(HttpMethod.DELETE)
        allowMethods.add(HttpMethod.PATCH)

        router.route().handler(CorsHandler.create("*")
                .allowedHeaders(allowHeaders)
                .allowedMethods(allowMethods))
    }

    /**
     * Enable local session storage in requests.
     *
     * @param router router instance
     */
    protected fun enableLocalSession(router: Router) {
        router.route().handler(CookieHandler.create())
        router.route().handler(SessionHandler.create(
                LocalSessionStore.create(vertx, "shopping.user.session")))
    }

    /**
     * Enable clustered session storage in requests.
     *
     * @param router router instance
     */
    protected fun enableClusteredSession(router: Router) {
        router.route().handler(CookieHandler.create())
        router.route().handler(SessionHandler.create(
                ClusteredSessionStore.create(vertx, "shopping.user.session")))
    }

    // Auth helper method

    /**
     * Validate if a user exists in the request scope.
     */


    // helper method dealing with failure

    protected fun badRequest(context: RoutingContext, ex: Throwable) {
        context.response().setStatusCode(400)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("error", ex.message).encodePrettily())
    }

    protected fun notFound(context: RoutingContext) {
        context.response().setStatusCode(404)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("message", "not_found").encodePrettily())
    }

    protected fun internalError(context: RoutingContext, ex: Throwable) {
        context.response().setStatusCode(500)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("error", ex.message).encodePrettily())
    }

    protected fun notImplemented(context: RoutingContext) {
        context.response().setStatusCode(501)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("message", "not_implemented").encodePrettily())
    }

    protected fun badGateway(ex: Throwable, context: RoutingContext) {
        ex.printStackTrace()
        context.response()
                .setStatusCode(502)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("error", "bad_gateway")
                        //.put("message", ex.getMessage())
                        .encodePrettily())
    }

    protected fun serviceUnavailable(context: RoutingContext) {
        context.fail(503)
    }

    protected fun serviceUnavailable(context: RoutingContext, ex: Throwable) {
        context.response().setStatusCode(503)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("error", ex.message).encodePrettily())
    }

    protected fun serviceUnavailable(context: RoutingContext, cause: String) {
        context.response().setStatusCode(503)
                .putHeader("content-type", "application/json")
                .end(JsonObject().put("error", cause).encodePrettily())
    }
}
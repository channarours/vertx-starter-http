package common

import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.circuitbreaker.CircuitBreakerOptions
import io.vertx.core.AbstractVerticle
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.impl.ConcurrentHashSet
import io.vertx.core.json.JsonObject
import io.vertx.core.logging.LoggerFactory
import io.vertx.servicediscovery.Record
import io.vertx.servicediscovery.ServiceDiscovery
import io.vertx.servicediscovery.ServiceDiscoveryOptions
import io.vertx.servicediscovery.types.HttpEndpoint

open class BaseMicroserviceVerticle: AbstractVerticle() {

    protected lateinit var discovery: ServiceDiscovery
    protected lateinit var circuitBreaker: CircuitBreaker

    protected var registeredRecords:ConcurrentHashSet<Record?> = ConcurrentHashSet<Record?>()

    companion object {
        private val LOG_EVENT_ADDRESS:String = "events.log"
        private val logger = LoggerFactory.getLogger(BaseMicroserviceVerticle::class.java)
    }

    override fun start() {
        //init service discovery instance
        discovery = ServiceDiscovery.create(vertx, ServiceDiscoveryOptions().setBackendConfiguration(config()))

        // init circuit breaker instance
        var circuitBreakerOptions = if (config().getJsonObject("circuit-breaker") != null) {
            config().getJsonObject("circuit-breaker")
        } else {
            JsonObject()
        }

        circuitBreaker = CircuitBreaker.create(circuitBreakerOptions.getString("name", "circuit-breaker"), vertx,
                CircuitBreakerOptions()
                        .setMaxFailures(circuitBreakerOptions.getInteger("max-failures", 5))
                        .setTimeout(circuitBreakerOptions.getLong("timeout", 10000L))
                        .setFallbackOnFailure(true)
                        .setResetTimeout(circuitBreakerOptions.getLong("reset-timeout", 30000L))
        )
    }

    protected fun publishHttpEndpoint(name:String, host:String, port:Int): Future<Void> {
        var record = HttpEndpoint.createRecord(name, host, port, "/",
                JsonObject().put("api.name", config().getString("api.name", ""))
                )

        return publish(record)
    }

    protected fun publishApiGateway(host:String, port:Int): Future<Void> {
        var record = HttpEndpoint.createRecord("api-gateway", true, host, port, "/", null).setType("api-gateway")

        return publish(record)
    }

    private fun publish(record: Record?): Future<Void> {

        if(discovery == null) {
            try {
                start()
            }catch (e: Exception) {
                throw IllegalStateException("Cannot create discovery service");
            }
        }

        var future = Future.future<Void>()

        // publish the service

        discovery.publish(record) { ar->

            if (ar.succeeded()) {
                registeredRecords.add(record)
                logger.info("Service <${ar.result().name}> published")
                future.complete()
            }else {
                future.fail(ar.cause())
            }
        }

        return future
    }

    override fun stop(stopFuture: Future<Void>?) {

        val futures = ArrayList<Future<Void>>()

        registeredRecords.forEach { record ->
            var cleanupFuture = Future.future<Void>()
            futures.add(cleanupFuture)
            this.discovery.unpublish(record!!.registration, cleanupFuture.completer())
        }

        if(futures.isEmpty()) {
            discovery.close()
            stopFuture?.complete()
        }else{
            CompositeFuture.all(futures.toList()).setHandler { ar->
                discovery.close()

                if (ar.failed()) {
                    stopFuture?.fail(ar.cause())
                }else {
                    stopFuture?.complete()
                }
            }
        }

    }
}
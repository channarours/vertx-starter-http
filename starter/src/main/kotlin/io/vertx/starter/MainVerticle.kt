package io.vertx.starter

import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.web.Router

class MainVerticle:AbstractVerticle() {
    override fun start() {
        val options = HttpServerOptions()
        options.port = config().getInteger("http.port", 8080)
        options.host = config().getString("http.address", "localhost")

        vertx.createHttpServer(options).requestHandler { router.accept(it) }.listen { ar->
            if (ar.succeeded()) {
                println("Server is now listening on ${options.host} : ${options.port}!")
            } else {
                print("Failed to bind!")
            }
        }
    }

    private val router = Router.router(vertx).apply {
        get("/").handler { ctx ->
            ctx.response().end("Welcome, Channa!")
        }
    }
}
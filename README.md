**Vertx microservice for starter**

## 1. Gradle Configuration

    plugins {
        id 'java'
        id 'org.jetbrains.kotlin.jvm' version '1.2.51'
        id 'application'
        id 'com.github.johnrengelman.shadow' version '2.0.4'
    }
    
    def mainVerticleName = 'io.vertx.starter.MainVerticle'
    def vertxVersion = '3.5.3'
    def watchForChange = 'src/**/*'
    def doOnChange = 'gradle classes'
    
    group 'secdev.com.kh'
    version '1.0'
    
    sourceCompatibility = 1.8
    mainClassName = 'io.vertx.core.Launcher'
    
    repositories {
        mavenCentral()
    }
    
    dependencies {
        compile "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
        implementation group: 'io.vertx', name: 'vertx-core', version: vertxVersion
        implementation group: 'io.vertx', name: 'vertx-web', version: vertxVersion
        testCompile group: 'junit', name: 'junit', version: '4.12'
    }
    
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    
    shadowJar{
        classifier = 'fat'
        manifest {
            attributes 'Main-Verticle': mainVerticleName
        }
        mergeServiceFiles{
            include 'META-INF/services/io.vertx.core.spi.VerticleFactory'
        }
    }
    run {
        args = ['run', mainVerticleName, "--redeploy=$watchForChange", "--launcher-class=$mainClassName", "--on-redeploy=$doOnChange", "-conf=src/conf/config.json"]
    }


## 2. Project Structure
![enter image description here](https://lh3.googleusercontent.com/_ReCegspZcuS67qZH66VWwLGLDLCWYFtyNfwjwPl0tT7ocp-svwaK-4BxKRUPitTpcRsHG_DSYoJ)
## 3.  Simple Verticle
**config.json**

    {  
	  "http.address": "127.0.0.1",  
	  "http.port": 8888  
	}
	
**MainVerticle.kt**
    
    package io.vertx.starter
    
    import io.vertx.core.AbstractVerticle
    import io.vertx.core.http.HttpServerOptions
    import io.vertx.ext.web.Router
    
    class MainVerticle:AbstractVerticle() {
        override fun start() {
            val options = HttpServerOptions()
            options.port = config().getInteger("http.port", 8080)
            options.host = config().getString("http.address", "localhost")
    
            vertx.createHttpServer(options).requestHandler { router.accept(it) }.listen { ar->
                if (ar.succeeded()) {
                    println("Server is now listening on ${options.host} : ${options.port}!")
                } else {
                    print("Failed to bind!")
                }
            }
        }
    
        private val router = Router.router(vertx).apply {
            get("/").handler { ctx ->
                ctx.response().end("Welcome!")
            }
        }
    }
    
## Build and Run verticle
**Gradle Build**
![gradle-build](https://lh3.googleusercontent.com/7yzcbrUN0yxz1xfg11OPacL4dQb9r-_HhQaOxVlAgcE7WxpSqIgEkAqwa_XnWi-CUaH7YWYlV0Ok)
	
**Gradle Run**
![gradle-run](https://lh3.googleusercontent.com/oMOQBih50g4GZ0lVXLwekKtCBINmiZqOnyiVh9ZcDzSMm1qupKsRZC3YJYQS9f5tZmQtmbKn23qo)

**Accessing to Server on browser**
![Accessing to server](https://lh3.googleusercontent.com/15YqtyLZfByhKMV9LKcpIiivKgbMVwCJgSg92jwOtDTCYyhEmafMxOW-VNhp6k0OPygBNMswfH3g)